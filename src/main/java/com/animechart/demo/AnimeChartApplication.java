package com.animechart.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnimeChartApplication {
	//ok ?
	public static void main(String[] args) {
		SpringApplication.run(AnimeChartApplication.class, args);
	}

}

package com.animechart.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.animechart.demo.model.Anime;
import com.animechart.demo.repository.AnimeRepositoryInterface;

 
@Controller
public class AnimeController {

	@Autowired
	private AnimeRepositoryInterface animeRepository;
	 
	@RequestMapping(value="/home")
	public ModelAndView home()
	{ 
		Iterable<Anime> animeList = animeRepository.findAll();
		return new  ModelAndView("anime/home","animeList",animeList);
	}
	
	@RequestMapping(value="/create")
	public ModelAndView create()
	{ 
		return new  ModelAndView("anime/create");
	}
	
	@RequestMapping(value="/store", method = RequestMethod.POST)
	public String store(@ModelAttribute("animeData") Anime anime, RedirectAttributes attributes)
	{
		 animeRepository.save(anime);
		 attributes.addFlashAttribute("success","Anime creato");
		 return "redirect:/home";
	}
	
	
	@RequestMapping(value="/edit", method = RequestMethod.POST)
	public ModelAndView editAnime(@ModelAttribute("animeIdEdit") int id, RedirectAttributes attributes)
	{
		Optional<Anime> foundAnime = animeRepository.findById(id); 
			
		Anime anime = foundAnime.get();
		
		return new ModelAndView("anime/edit","anime",anime); //se invece di mettere editAnime, lo metti come redirect:/allAnime, non funzionerà, il modelandView non funziona con i redirect url
	}
	
	@RequestMapping(value="/update", method = RequestMethod.POST)
	public RedirectView updateAnime(@ModelAttribute("animeData") Anime anime, RedirectAttributes attributes)
	{
		Optional<Anime> foundAnime = animeRepository.findById(anime.getId());
		if(foundAnime.isEmpty())
		{

		    attributes.addFlashAttribute("error","Errore modifica anime");
			return  new RedirectView("home");
		}
		else
		{
			foundAnime.get().setName(anime.getName());
			foundAnime.get().setGenere(anime.getGenere());
			foundAnime.get().setTrama(anime.getTrama());
			
			animeRepository.save(foundAnime.get());
			
		    attributes.addFlashAttribute("success","Anime modificato con successo");
			    
			return  new RedirectView("home");
		}
		
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.POST)
	public RedirectView deleteAnime(@ModelAttribute("animeIdElimina") int id, RedirectAttributes attributes)
	{
			Optional<Anime> foundAnime = animeRepository.findById(id);
			if(foundAnime.isEmpty())
			{
			    attributes.addFlashAttribute("error","errore eliminazione");
			    
				return new  RedirectView("/home");
			}
			else
			{
				animeRepository.delete(foundAnime.get());
				
				attributes.addFlashAttribute("success","Anime eliminato con successo");
				
				return new  RedirectView("/home");
			}
		
	}
	
}

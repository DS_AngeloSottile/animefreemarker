package com.animechart.demo.controller;

import java.util.Optional;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.animechart.demo.exception.AnimeNotFoundException;
import com.animechart.demo.model.Anime;
import com.animechart.demo.repository.AnimeRepositoryInterface;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping("/api")
public class AnimeControllerApi {

	@Autowired
	private AnimeRepositoryInterface animeRepository;
	
	
	@RequestMapping(value="/animes", method = RequestMethod.GET,produces = "application/json")
	public ResponseEntity<Iterable<Anime>> index()
	{  
		//return new ResponseEntity<Iterable<Anime>>(animeRepository.getAllCustom("Anime2"), HttpStatus.OK);
		Iterable<Anime> animes = animeRepository.findAll();
		int size = (int) StreamSupport.stream(animes.spliterator(), false).count();
		if ( size == 0 )
		{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Non trovato");
		}
		return new ResponseEntity<Iterable<Anime>>(animes, HttpStatus.OK);
	}
	
	@RequestMapping(value="/anime/{id}",  method = RequestMethod.GET)
	public Anime show(@PathVariable int id)
	{ 
		 Optional<Anime> anime =  animeRepository.findById(id);
		 
		 if(anime.isEmpty())
		 {
			 throw new ResponseStatusException(HttpStatus.NOT_FOUND,"item not found"); 
		 }
		 
		 return anime.get();
	}
	
	@RequestMapping(value="/anime/store", method = RequestMethod.POST)
	public Anime store(@RequestBody Anime anime)
	{
		 return animeRepository.save(anime); 
	}
	
	@RequestMapping(value="/anime/update/{id}", method = RequestMethod.PUT)
	public Anime updateAnime(@PathVariable int id, @RequestBody Anime anime)
	{
		Optional<Anime> foundAnime = animeRepository.findById(id);
		if(foundAnime.isEmpty())
		{
			 throw new ResponseStatusException(HttpStatus.NOT_FOUND,"item not found"); 
		}
		else
		{
			foundAnime.get().setName(anime.getName());
			foundAnime.get().setGenere(anime.getGenere());
			foundAnime.get().setTrama(anime.getTrama());
			
			animeRepository.save(foundAnime.get());
			    
			return  foundAnime.get();
		}
		
	}
	
	@RequestMapping(value="/anime/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteAnime(@PathVariable int id) throws AnimeNotFoundException
	{
			Optional<Anime> foundAnime = animeRepository.findById(id);
			if(foundAnime.isEmpty())
			{
				throw new AnimeNotFoundException("Anime non trovato custom");
				//throw new ResponseStatusException(HttpStatus.NOT_FOUND,"item not found"); 
			}
			else
			{
				ObjectMapper mapper = new ObjectMapper();
				ObjectNode node = mapper.createObjectNode();
				
				
				animeRepository.delete(foundAnime.get());
				
				node.put("code", HttpStatus.OK.toString());
				node.put("message","Eliminazione effettuata");
		
				return new ResponseEntity<>(node, HttpStatus.OK);
			}
		
	}

}

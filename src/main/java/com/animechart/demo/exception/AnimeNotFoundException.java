package com.animechart.demo.exception;

public class AnimeNotFoundException  extends Exception{
  
	private static final long serialVersionUID = -9165720251062110782L;

	private String message;

	
	public AnimeNotFoundException() 
	{
		super();
	}
	
	
	public AnimeNotFoundException(String message) 
	{
		super();
		this.message = message;
	}
	
	
	public String getMessage() 
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	
	
}

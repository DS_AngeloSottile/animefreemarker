package com.animechart.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(AnimeNotFoundException.class)
	public ResponseEntity<ErrorResponse> AnimeNotFoundHandler(Exception e)
	{
		ErrorResponse error = new ErrorResponse();
		
		error.setCode(HttpStatus.NOT_FOUND.value());
		error.setMessage(e.getMessage());
		
		
		return new ResponseEntity<ErrorResponse>(error,HttpStatus.NOT_FOUND);
	}
}

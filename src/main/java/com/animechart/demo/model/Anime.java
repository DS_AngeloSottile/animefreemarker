package com.animechart.demo.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="anime")
public class Anime {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String name;
	
	private String genere;
	
	private String trama;
	
	private String cover;

	public Anime()
	{
		
	}
	public Anime(int id, String name, String genere, String trama, String cover) 
	{
		super();
		this.id = id;
		this.name = name;
		this.genere = genere;
		this.trama = trama;
		this.cover = cover;
	}
	
	

	public int getId()
	{
		return id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	
	public String getName() 
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	
	public String getGenere() 
	{
		return genere;
	}

	public void setGenere(String genere) 
	{
		this.genere = genere;
	}

	
	
	public String getTrama() 
	{
		return trama;
	}

	public void setTrama(String trama)
	{
		this.trama = trama;
	}

	
	public String getCover() 
	{
		return cover;
	}

	public void setCover(String cover) 
	{
		this.cover = cover;
	}
	
	
}

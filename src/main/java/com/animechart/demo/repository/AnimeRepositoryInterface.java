package com.animechart.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.animechart.demo.model.Anime;

public interface AnimeRepositoryInterface extends CrudRepository<Anime, Integer>{
	
	@Query("SELECT name FROM Anime WHERE name = ?1")
	public Iterable<Anime> getAllCustom(String name);

}

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="./css/animestyle.css">
		<title>ANIME</title>
	<head>
	<body>
	
	<header id="header">
			<div class="logo"></div>
			<nav id="main-menu">
				<ul>
					<li> <div class="cv"> <a class="winter" href="home">Admin Home</a></div></li>
				</ul>
			</nav>
			<div class="logob"></div>
	</header>
	
		<h1> Modifica Anime</h1> 
		<form action="update" method="post" id="animeData">
			<input type="hidden" name="id" value="${anime.id}">
		
			<article class="boxanime ">
			
				<div class="boxtitolo"><div class="cv"><input type="text" name="name" value="${anime.name}"></div></div>       
				<div class="boximg"><img style=" width: 100%; height:100%;" src="${anime.cover}"></div>
				<div class="boxinfo">
					<div class="boxgenere"><div class="cv"><b><input type="text" name="genere" value="${anime.genere}"></b></div></div>
					<div class="boxtrama"><b><input type="text" name="trama" value="${anime.trama}"></b></div>
				</div>
				<button type="submit" >Conferma modifiche</button> 
				
			</article>
			
		</form>
		
		
	<body>

</html>
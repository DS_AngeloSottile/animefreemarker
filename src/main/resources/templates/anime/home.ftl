<!DOCTYPE html>
<html>
	<head>
		
	
		<link rel="stylesheet" href="./css/animestyle.css">
		
		<title>Anime main page</title>
		
	</head>

	<body>
	
		<#if success??> 
			<h1> ${success} </h1>
		</#if>
		<#if error??> 
			<h1> ${error} </h1>
		</#if>
		
	
	<header id="header">
			<div class="logo"></div>
			<nav id="main-menu">
				<ul>
					<li> <div class="cv"> <a class="winter" href="home">Admin Home</a></div></li>
					<li> <div class="cv"> <a class="winter" href="create">Aggiungi Anime</a></div></li>
				</ul>
			</nav>
			<div class="logob"></div>
	</header>
		
		<div id="boxPrincipale">
			<#list animeList as anime>
				<article class="boxanime ">
					  
						<div class="boxtitolo"> ${anime.name}<div class="cv"></div></div>
						<div class="boximg"> <img style=" width: 100%; height:100%;" src="${anime.cover}"></div>
						<div class="boxinfo">
							<div class="boxgenere">${anime.genere} <div class="cv"><b></b></div></div>
							<div class="boxtrama"> ${anime.trama} <b></b></div>
						</div>
						<form  method="post" action="edit" id=""> 
							<input type="hidden" name="animeIdEdit" value="${anime.id}">  
							<button type="submit">Modifica Anime</button> 
						</form>
						<form  method="post" action="delete" id=""> 
							<input type="hidden" name="animeIdElimina" value="${anime.id}">  
							<button type="submit" id="deleteAnime">Elimina</button> 
						</form>
						
				</article>
		
			</#list>
		
		</div>
	
	</body>
</html>
	
